<!DOCTYPE html>
<html>
<head>
	<title>Jeevan</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link href="https://fonts.googleapis.com/css?family=Kanit|Oswald:700&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Cabin+Sketch|Oswald:700&display=swap" rel="stylesheet">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/semantic.min.css" integrity="sha256-9mbkOfVho3ZPXfM7W8sV2SndrGDuh7wuyLjtsWeTI1Q=" crossorigin="anonymous" /> 
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:700|Source+Sans+Pro:600&display=swap" rel="stylesheet">
	<script  src="{{asset('js/app.js')}}"></script>
	 <link rel="stylesheet" href="{{asset('css/grids.css')}}">
    <link rel="stylesheet" href="{{asset('css/newhome.css')}}">
    <link rel="stylesheet" href="{{asset('css/vertical-navbar.css')}}">
</head>
<body>
	<div class="header">
		@section('header')
					<div class="total-nav ">
			   <div class="content">
			      <div id="nav-grid" class="first-nav">
			        <div class="grid-item">
			          <h1 id="heading"><i class="key icon brand"></i>Antitheft System</h1>
			          </div>
			        <div class="contacts">
			          <span class="phn-no" id="first-no"><strong>Sales: 0000-000-0000</strong></span>
			            <br>
			          <span class="phn-no" id="sec-no"><strong>24/7 Support: 0000-000-0000</strong> </span>
			          <a id="login" class="ui right labeled icon button small" href="login"><i class="sign-in icon"></i>Login</a>
			        </div>
			        <div class="grid-item">
			        <i id="open" onclick="openNav()" class="align right icon"></i>
			        <i id="close" onclick="closeNav()" class="nav-click close icon"></i>
			        </div>
			      </div>
			    </div>
			    <div id="navbar" class="content">
			     <div class="ui secondary stackable menu">
			        <a href="home" class="item">
			          Home
			        </a>
			        <a class="item">
			          Subscribe
			        </a>
			        <a class="item">
			          About
			        </a>
			        <a class="item">
			          Help
			        </a>
			        <a class="item">
			          Contact
			        </a>
			        <a href="login" class="item login-btn">
			          Login
			        </a>
			      </div>
			    </div>
			</div>
		@show()
	</div>
	<div class="content">
		@section('content')
		@show()
	</div>
	<div class="footer">
		@section('footer')
			<style>
	        label{
	          color:#000 !important;
	          font-weight: 1000 !important;
	        }
     		 </style>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <div id="footer">
      <div id="links">
        <div id="icons grid-item">
        <a href="#"><i class="facebook icon links"></i></a>
        <a href="#"><i class="instagram icon links"></i></a>
        <a href="#"><i class="twitter icon links"></i></a>
      </div>
      <div id="foo-note" class="grid-item">
      <p id="foot-note">Copyright &copy; -----</p>
      </div>
        
      </div>
    </div>
    <script src="{{asset('js/home.js')}}"></script>

		@show()
	</div>



</body>
</html>